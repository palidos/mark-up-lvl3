window.onload = function(){
    feedBack = document.getElementById('fdbk');
    if(feedBack){
        var opened = false;
        feedBack.addEventListener("click", () => {
            if (opened === false){
            feedBackForm = document.getElementById('fbf');
            $('#fbf').css('visibility','visible').hide().fadeIn();
            var form = document.createElement('form'); // Create New Element Form
            form.setAttribute("action", ""); // Setting Action Attribute on Form
            form.setAttribute("method", "post"); // Setting Method Attribute on Form
            feedBackForm.appendChild(form);


            var closeButton = document.createElement('button');
            closeButton.className = "closeBTN";
            closeButton.innerHTML = "×";
            closeButton.addEventListener("click", (event) => {
                event.preventDefault();
                if ($("#fbf").is(":visible")){$("#fbf").fadeOut();}
                //setTimeout(0, function(){
                    while (feedBackForm.firstChild) {
                        feedBackForm.removeChild(feedBackForm.firstChild);
                    }
                //});
                opened = false;
                return false;
            });
            form.appendChild(closeButton);


            var heading = document.createElement('h2'); // Heading of Form
            heading.innerHTML = "ОБРАТНАЯ СВЯЗЬ";
            form.appendChild(heading);

            var linebreak = document.createElement('br');
            form.appendChild(linebreak);



            var namelabel = document.createElement('label'); // Create Label for Name Field
            namelabel.innerHTML = "Ваше имя"; // Set Field Labels
            form.appendChild(namelabel);

            var inputelement = document.createElement('input'); // Create Input Field for Name
            inputelement.setAttribute("type", "text");
            inputelement.setAttribute("name", "dname");
            inputelement.setAttribute("required", "required");
            inputelement.setAttribute("placeholder", "Поле обязательно для заполнения");
            form.appendChild(inputelement);

            var linebreak = document.createElement('br');
            form.appendChild(linebreak);



            var namelabel = document.createElement('label'); // Create Label for Name Field
            namelabel.innerHTML = "Телефон"; // Set Field Labels
            form.appendChild(namelabel);

            var inputelement = document.createElement('input'); // Create Input Field for Name
            inputelement.setAttribute("type", "text");
            inputelement.setAttribute("name", "dname");
            inputelement.setAttribute("placeholder", "Поле обязательно для заполнения");
            inputelement.setAttribute("required", "required");
            form.appendChild(inputelement);

            var linebreak = document.createElement('br');
            form.appendChild(linebreak);



            var emaillabel = document.createElement('label'); // Create Label for E-mail Field
            emaillabel.innerHTML = "Email";
            form.appendChild(emaillabel);

            var emailelement = document.createElement('input'); // Create Input Field for E-mail
            emailelement.setAttribute("type", "text");
            emailelement.setAttribute("name", "demail");
            emailelement.setAttribute("placeholder", "Поле обязательно для заполнения");
            emailelement.setAttribute("required", "required");
            form.appendChild(emailelement);

            var emailbreak = document.createElement('br');
            form.appendChild(emailbreak);



            var messagelabel = document.createElement('label'); // Append Textarea
            messagelabel.innerHTML = "Сообщение";
            form.appendChild(messagelabel);

            var texareaelement = document.createElement('textarea');
            texareaelement.setAttribute("name", "dmessage");
            texareaelement.setAttribute("placeholder", "Поле обязательно для заполнения");
            texareaelement.setAttribute("required", "required");
            form.appendChild(texareaelement);

            var messagebreak = document.createElement('br');
            form.appendChild(messagebreak);

            var submitelement = document.createElement('input');
            $(submitelement).click((event) =>{
                event.preventDefault(); 
            });
            // Append Submit Button
            submitelement.className = "submit";
            submitelement.setAttribute("type", "submit");
            submitelement.setAttribute("name", "dsubmit");
            submitelement.setAttribute("value", "Submit");
            submitelement.value="ОТПРАВИТЬ";
            form.appendChild(submitelement);
            opened = true;

            submitelement.addEventListener("click", (event) => {
                event.preventDefault();
                // if ($("#fbf").is(":visible")) {
                //     $("#fbf").fadeOut();
                // }
                // //setTimeout(0, function(){
                //     while (feedBackForm.firstChild) {
                //         feedBackForm.removeChild(feedBackForm.firstChild);
                //     }
                // //});
                // opened = false;
                // return false;
            });
        }
        });
    }
}
